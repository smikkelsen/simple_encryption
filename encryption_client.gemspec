# -*- encoding: utf-8 -*-
require File.expand_path('../lib/encryption_client/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Sean Mikkelsen"]
  gem.email         = ["smikkelsen@myebiz.com"]
  gem.description   = %q{client for using encryption/decryption methods}
  gem.summary       = %q{Encryption Libraries}
  gem.homepage      = ""

  gem.add_runtime_dependency "rest-client", ">= 1.6.7"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "encryption_client"
  gem.require_paths = ["lib"]
  gem.version       = EncryptionClient::VERSION
end