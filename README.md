# EncryptionClient

The purpose of this gem is to centralize all of our encryption libraries used with our partners (or internally). Just include the gem and encrypt/decrypt using the desired library.

## Installation

Add this line to your application's Gemfile:

    gem 'encryption_client'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install encryption_client

## Usage

All Modules are namespaced:
    EncryptionClient

To use an encryption library, you can do something like this:
    EncryptionClient::Aes256.encrypt("Some string to encrypt", "key", "iv")

Decrypting:
    EncryptionClient::Aes256.decrypt("Some string to decrypt", "key", "iv")

Base 64 encode or decode strings:
    EncryptionClient::Aes256.encode("Some string to encode")
    EncryptionClient::Aes256.decode("Some string to decode")

Encrypt and Encode all at once:
    EncryptionClient::Aes256.query_prep("Some string to encrypt, then encode", "key", "iv")

Decode and Decrypt all at once:
    EncryptionClient::Aes256.response_prep("Some string to decode, then encrypt", "key", "iv")

You can also set your key and iv once, so you don't have to pass them in each time:
        EncryptionClient::Aes256.key="oijlkmwme98jz9p8up2wjhkajsf"
        EncryptionClient::Aes256.iv="kjaoijhnowineoiaui"
        EncryptionClient::Aes256.query_prep("Some string to encrypt, then encode")
        EncryptionClient::Aes256.response_prep("Some string to decode, then encrypt")

Generate a key or iv:
        EncryptionClient::Aes256.generate_key
        EncryptionClient::Aes256.generate_iv


You an also

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request