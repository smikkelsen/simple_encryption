require "encryption_client/version"
# Required for AES-128-CBC Encryption/Decryption
require 'openssl'
require 'base64'
require 'encryption_client'

module Aes128
  extend self
  extend EncryptionClient

  @key = nil
  @iv = nil

  def key()
    @key
  end

  def key=(key)
    @key = key
  end

  def iv()
    @iv
  end

  def iv=(iv)
    @iv = iv
  end

  def query_prep(query, muddle=false, key=@key, iv=@iv)
    if key.nil? || iv.nil?
      error_log "Key OR IV not set"
      return false
    end
    query = encrypt(query, key, iv)
    if muddle
      query = muddle query, iv
    end
    encode(query)
  end

  def response_prep(response, demuddle=false, iv_len=16, key=@key, iv=@iv)
    decoded = decode(response)

    if demuddle
      iv = demuddle_iv decoded, iv_len
      decoded = demuddle_string decoded, iv_len
    end

    if key.nil? || iv.nil?
      error_log "Key OR IV not set"
      return false
    end

    decrypt(decoded, key, iv)
  end

  def encrypt(string, key=@key, iv=@iv)
    if key.nil? || iv.nil?
      error_log "Key OR IV not set"
      return false
    end
    cipher = OpenSSL::Cipher::AES128.new(:CBC)
    cipher.encrypt
    cipher.key = key
    cipher.iv = iv
    cipher.update(string) + cipher.final
  end

  def decrypt(string, key=@key, iv=@iv)
    if key.nil? || iv.nil?
      error_log "Key OR IV not set"
      return false
    end
    cipher = OpenSSL::Cipher::AES128.new(:CBC)
    cipher.decrypt
    cipher.key = key
    cipher.iv = iv
    cipher.update(string) + cipher.final
  end

end