require "encryption_client/version"
# Required for AES-256-CBC Encryption/Decryption
require 'openssl'
require 'base64'

module EncryptionClient
  extend self

  def muddle(string, iv)
    len = iv.length
    if len % 2 > 0
      iv = "#{iv}0"
      len = iv.length
    end
    half = len / 2
    "#{iv.slice(half..len-1)}#{iv.slice(0..half-1)}#{string}"
  end

  def demuddle_iv(string, len)
    if len % 2 > 0
      len += 1
      half = len / 2
      "#{string.slice(half..len-1)}#{string.slice(0..half-1)}".chop
    else
      half = len / 2
      "#{string.slice(half..len-1)}#{string.slice(0..half-1)}"
    end
  end

  def demuddle_string(string, len)
    if len % 2 > 0
      len += 1
    end
    string.slice(len..string.length-1)
  end

  def encode(query)
    Base64.urlsafe_encode64(query).strip
  end

  def decode(query)
    Base64.urlsafe_decode64(query)
  end

  def generate_key(len = 32, inc_num = true, inc_sym = true, symbols = nil)
    gen_random_string len, inc_num, inc_sym, symbols
  end

  def generate_iv(len = 16, inc_num = true, inc_sym = true, symbols = nil)
    gen_random_string len, inc_num, inc_sym, symbols
  end

  def gen_random_string(len, inc_num = true, inc_sym = true, symbols = nil)
    #(0...len).collect { rand(36).to_s(36) }.map { |x|
    #  (rand<0.55) ? x : x.upcase }.join
    symbols ||= %w[` ~ ! @ # $ % ^ & * ( ) _ - + = { } [ ] \\ | : ; " ' < > , . ? /]
    randomize = Array.new
    randomize << ('a'..'z')
    randomize << ('A'..'Z')
    randomize << (0..9) if inc_num
    randomize << symbols if inc_sym

    o = randomize.map { |i| i.to_a }.flatten
    string = (0...len).map { o[rand(o.length)] }.join
  end

  private

  def error_log(heading, print=nil)
    Rails.logger.debug(" ======================================================== ")
    Rails.logger.debug("     ENCRYPTION_CLIENT -   #{heading}                     ")
    unless print.nil?
      Rails.logger.debug(" -------------------------------------------------------- ")
      Rails.logger.debug(print)
    end
    Rails.logger.debug(" ======================================================== ")
  end

end